//create context menu entries for ReadLater

//update badge on tab change
chrome.tabs.onActivated.addListener(function(tab) {

  chrome.storage.local.get({'tabURL': []}, function(items) {

    if (items.tabURL) {
      var latestFetchedURL = [];
      latestFetchedURL = items.tabURL;

      for(var i = 0; i < latestFetchedURL.length; i++)
      {
        if(latestFetchedURL[i] === tab.url)
        {
          chrome.browserAction.setBadgeText({text: "KICK"});
          return;
        }
      }
      chrome.browserAction.setBadgeText({text: "ADD"});
    }
    else {
      alert("FAILED");
    }
  });

});

var contextNorth = chrome.contextMenus.create(
  {
    "title": "Add page to ReadLater",
    "onclick": function (evt)
    {

      alert("You clicked north");
        //chrome.tabs.create({ url: evt.pageUrl });

    }
    });

var contextSouth = chrome.contextMenus.create(
      {
        "title": "Remove page from ReadLater",
        "onclick": function (evt)
        {
        alert("South "+evt.pageUrl);

    }
        });


        //check if entry exists already
        chrome.browserAction.onClicked.addListener(function(tab) {

          chrome.storage.local.get({'tabURL': []}, function(items) {

            if (items.tabURL) {
              var latestFetchedURL = [];
              latestFetchedURL = items.tabURL;

              for(var i = 0; i < latestFetchedURL.length; i++)
              {
                if(latestFetchedURL[i] === tab.url)
                {
                  alert("This URL has been previously been added to ReadLater");
                  chrome.browserAction.setBadgeText({text: "KICK"});
                  return;
                }
              }

              //add entry to ReadLater
              var blueObjectHolder = {'tabURL': []};
              blueObjectHolder.tabURL.push(tab.url);

              chrome.storage.local.set(blueObjectHolder, function() {
              alert("Successfully added URL to the ReadLater list");
              chrome.browserAction.setBadgeText({text: "KICK"});
              });

            }
            else {
              alert("FAILED");
            }
          });

        });
